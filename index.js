'use strict';
var http = require('http'),
    fs = require('fs');
var less = require('less'),
    Promise = require('bluebird');
var App = require('./src/app');
var app = new App();

/**
 * @param {string} filename
 * @return {Promise.<string>}
 */
function renderLess(filename) {
  return new Promise(function (resolve, reject) {
    fs.readFile(filename, { encoding: 'utf8' }, function (err, data) {
      if (err) { return reject(err); }
      try {
        less.render(data, function (err, css) {
          if (err) { return reject(err); }
          resolve(css);
        });
      } catch (err) {
        reject(err);
      }
    });
  });
}

app.get('/', function (request, response) {
  return this.sendFile('public/index.html', request, response);
});

app.get('/public/stylesheets/calc.css', function (request, response) {
  var params = this.params(request);

  return renderLess(params.filename.replace(/\.css$/, '.less'))
    .then(function (css) {
      response.writeHead(200, { 'Content-Type': 'text/css' });
      response.write(css);
      response.end();
    });
});

http.createServer(app.transact.bind(app)).listen(3000);
console.log('Listening port 3000.');
