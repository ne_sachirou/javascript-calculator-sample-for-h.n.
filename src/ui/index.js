/**
 * @license GPLv3 ne_Sachirou <utakata.c4se@gmail.com>
 */
(function (global) {
/* global Calc */
'use strict';

if (!Array.from) {
  Array.from = function (obj) {
    return [].slice.call(obj);
  };
}

/**
 * @constructor
 */
var CalcUi = function () {
  CalcUi = function () { return this; };
  this.calc = new Calc();
  this.calcNode = document.getElementById('calc');
  this.calcDisplay = this.calcNode.getElementsByClassName('calc-display')[0];
};

/**
 * @return {CalcUi}
 */
CalcUi.prototype.start = function () {
  var _this = this;

  this.calcDisplay.contentEditable = true;
  Array.from(this.calcNode.querySelectorAll('.calc-body button')).forEach(function (button) {
    button.onclick = function () {
      // _this.calc.formula = _this.calcDisplay.textContent;
      if (button.textContent === '=') {
        _this.calc.evaluate();
      } else {
        _this.calc.pushToFormula(button.textContent);
      }
      _this.calcDisplay.textContent = _this.calc.formula;
    };
  });
  Object.observe(this.calc, function (evt) {
    console.log(evt);
  });
  new MutationObserver(function () {
    // _this.calc.formula = _this.calcDisplay.textContent;
  }).observe(this.calcDisplay, { childList: true });
  return this;
};

global.CalcUi = CalcUi;
}(this.self || global));
