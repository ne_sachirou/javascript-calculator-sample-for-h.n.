/**
 * @license GPLv3 ne_Sachirou <utakata.c4se@gmail.com>
 */
(function (global) {
'use strict';

/**
 * @constructor
 */
function Calc() {
  this._formula = [];
}

Object.defineProperty(Calc.prototype, 'formula', {
  /**
   */
  get: function () {
    return this._formula.map(function (token) { return token[1]; }).join('');
  }
});

/**
 */
Calc.prototype.pushToFormula = function (chr) {
  var last = this._formula[this._formula.length - 1];

  function token(chr) {
    /* jshint maxcomplexity: 100 */
    switch (chr) {
      case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
        return ['num', parseInt(chr)];
      case '+': case '-': case '*': case '/':
        return ['op', chr];
      case '=':
        this.evaluate();
        return;
      default:
        throw new Error('Unknown token type: ' + chr);
    }
  }

  function pushToFormula(chr) {
    if (chr[0] === 'op') {
      if (!last && chr[1] !== '+' && chr[1] !== '-') { return; }
      if (last && last[0] === 'op') { this._formula.pop(); }
      this._formula.push(chr);
    } else {
      if (!last || (last && last[0] === 'op')) {
        return this._formula.push(chr);
      }
      this._formula.pop();
      chr[1] = last[1] * 10 + chr[1];
      this._formula.push(chr);
      return;
    }
  }

  chr = token(chr);
  if (!chr) { return this; }
  pushToFormula.call(this, chr);
  return this;
};

/**
 */
Calc.prototype.evaluate = function () {
  /* jshint evil: true */
  this._formula = [['num', eval(this.formula)]];
  return this;
};

global.Calc = Calc;
}(this.self || global));
