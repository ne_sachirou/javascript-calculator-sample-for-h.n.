'use strict';
var concat = require('gulp-concat'),
    gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');
var jsSrc = ['*.js', 'src/**/*.js'],
    uglifyjsSrc = ['public/bower_components/observe-shim/lib/observe-shim.js', 'src/calc/*.js', 'src/ui/*.js'];

function doJshint(src) {
  src.pipe(jshint()).
    pipe(jshint.reporter('default'));
}

function doUglifyjs(src) {
  src.
    pipe(concat('public/javascripts/calc.min.js')).
    pipe(uglify({ outSourceMap: true })).
    pipe(gulp.dest('.'));
}

gulp.task('jshint', function () {
  doJshint(gulp.src(jsSrc));
});

gulp.task('uglifyjs', function () {
  doUglifyjs(gulp.src(uglifyjsSrc));
});

gulp.task('watch', function () {
  gulp.src(jsSrc).
    pipe(watch(function (files) {
      doJshint(files);
    }));
  gulp.src(jsSrc).
    pipe(watch(function (files) {
      doUglifyjs(files);
    }));
});

gulp.task('test', ['jshint']);
gulp.task('build', ['uglifyjs']);
gulp.task('default', ['build', 'test']);
